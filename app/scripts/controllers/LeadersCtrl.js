angular.module('clientApp')
    .controller('LeadersCtrl', ['$scope', '$state', 'AdminService', '$mdDialog', 
    					function($scope, $state, AdminService, $mdDialog) {

        $scope.leaders = [];

        var getleaders = function() {
            AdminService.getLeaders().then(function(response) {
                $scope.leaders = response;
                for (var i = 0, len = $scope.leaders.length; i < len; i++) {
                    $scope.leaders[i].imagePath = "app/images/communityLeaders/" + $scope.leaders[i].imagePath;
                }
            })
        };

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                console.log(item)
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }
        
        getleaders();

        $scope.edit = function(leaders) {
            $state.go('editLeader', {
                'id': leaders.id,
                'leader': leaders
            });
        }

    }])
    .controller('AddLeadersCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', function($scope, $state, AdminService, AlertService, $stateParams) {

        $scope.user = {

        };

        $scope.add = function() {
            $scope.isLoading = true;
            if ($scope.isNew) {
                AdminService.addLeaders($scope.user, $scope.logo.image).then(function(success) {
                    AlertService.alert("Addded Successfully")
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            } else {
                AdminService.updateLeader('updateCommunityLeaderDetails', $scope.user).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })

            }

        };

        $scope.back = function() {
            $state.go('leaders');
        }

        var init = function() {
            if ($stateParams.id) {
                $scope.isNew = false;
                $scope.user = $stateParams.leader;
                $scope.headerText = "Update Leader";
                    $scope.buttonText = "Update";
            } else {
                $scope.isNew = true;
                $scope.headerText = "Add Leader";
                    $scope.buttonText = "Add";
            }
        };
        init();
    }])
