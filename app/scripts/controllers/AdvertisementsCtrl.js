angular.module('clientApp')
    .controller('AdvertisementsCtrl', ['$scope', '$state', 'AdminService', '$mdDialog', function($scope, $state, AdminService, $mdDialog) {

        $scope.adds = [];
        var getAdds = function() {
            AdminService.getAdds().then(function(response) {
                $scope.adds = response;
                for (var i = 0, len = $scope.adds.length; i < len; i++) {
                    $scope.adds[i].imagePath = "app/images/advertisement/"+$scope.adds[i].imagePath;
                    if(!$scope.adds[i].startDate){
                        $scope.adds[i].startDate = new Date();
                    }
                    if(!$scope.adds[i].endDate){
                        $scope.adds[i].endDate = new Date();
                    }
                }
            }, function(error) {

            })
        };
        
        $scope.editAdvertisement = function(advertisement) {
            $state.go('editAdvertisement', {
                'id': advertisement.id,
                'advertisement': advertisement
            });
        };

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                AdminService.deleteAdd(item.id).then(function(response) {
                    if (response.status == 'success') {
                        AlertService.alert("Deleted Successfully", 'md-primary');
                        getAdds();
                    } else {
                        AlertService.alert("Error deleting", 'md-warn');
                    }
                })
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }

        var init = function() {
            getAdds();
        };
        init();
    }])
    .controller('AddAdvertisementsCtrl', ['$scope', '$state', 'AdminService', 'AlertService','$stateParams', function($scope, $state, AdminService, AlertService,$stateParams) {
        $scope.user = {};
        $scope.isLoading = false;
        $scope.isNew = true;
        $scope.logo = {};
        $scope.add = function() {
            //$scope.isLoading = true;
            /*var obj = {
                "advertisementType": $scope.user.advertisementType,
                "advertisementCost": $scope.user.advertisementCost,
                "startDate": moment($scope.startDate).format('YYYY-MM-DD'),
                "endDate": moment($scope.endDate).format('YYYY-MM-DD'),
                "expired": false
            }*/
            if ($scope.isNew) {
            	AdminService.addAdds($scope.user, $scope.logo.image).then(function(success) {
                    AlertService.alert("Addded Successfully");
                    $scope.isLoading = false;
                    $scope.back();
                }, function(error) {
                    console.log("Error adding Adds");
                    $scope.isLoading = false;
                })
            } else {
                if($scope.logo.image){
                   // delete $scope.user.imagePath;
                }
            	AdminService.updateAdds($scope.user, $scope.logo.image).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            }
        };
        
        $scope.back = function() {
            $state.go('advertisements');
        }

        var init = function() {
            if ($stateParams.id) {
                if ($stateParams.advertisement) {
                    $scope.isNew = false;
                    $scope.user = $stateParams.advertisement;
                    $scope.user.imagePath = $scope.user.imagePath.replace('app/images/advertisement/', '');
                    if ($stateParams.advertisement.startDate) {
                        $stateParams.advertisement.startDate = new Date($stateParams.advertisement.startDate);
                    }
                    if ($stateParams.advertisement.endDate) {
                        $stateParams.advertisement.endDate = new Date($stateParams.advertisement.endDate);
                    }
                    $scope.headerText = "Update Advertisement";
                    $scope.buttonText = "Update";
                } else {
                    $scope.back();
                }
            } else {
                $scope.isNew = true;
                $scope.headerText = "Add Advertisement";
                    $scope.buttonText = "Add";
            }
        };
        init();
    }]);
