angular.module('clientApp')
    .controller('AddEducationCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', function($scope, $state, AdminService, AlertService, $stateParams) {

        $scope.user = {};
        $scope.isNew = true;
        $scope.isLoading = false;

        $scope.add = function() {
            $scope.isLoading = true;
            if ($scope.isNew) {
                AdminService.addEducation($scope.user).then(function(success) {
                    AlertService.alert("added Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            } else {
                $scope.user.helpingHandType = 'EDUCATION';
                AdminService.updateHelping('editEducationHelpingHands', $scope.user).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            }
        };


        $scope.back = function() {
            $state.go('education');
        };

        var init = function() {
            if ($stateParams.id) {
                if ($stateParams.education) {
                    $scope.isNew = false;
                    $scope.user = $stateParams.education;
                    $scope.user.phoneNumber = parseInt($scope.user.phoneNumber);
                    $scope.headerText = "Update Education";
                    $scope.buttonText = "Update";
                } else {
                    $scope.back();
                }
            } else {
                $scope.headerText = "Add Education";
                    $scope.buttonText = "Add";
                $scope.isNew = true;
            }
        };
        init();

    }])
    .controller('EducationCtrl', ['$scope', 'AdminService', '$state', '$mdDialog', 'AlertService', function($scope, AdminService, $state, $mdDialog, AlertService) {

        var getEducation = function() {
            AdminService.getEducation().then(function(response) {
                $scope.educationArr = response;
            }, function(error) {

            })
        };

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                AdminService.deleteEducation(item.id).then(function(response) {
                    if (response.status == 'success') {
                        AlertService.alert("Deleted Successfully", 'md-primary');
                        getEducation();
                    } else {
                        AlertService.alert("Error deleting", 'md-warn');
                    }
                })

            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }

        $scope.edit = function(education) {
            $state.go('editEducation', {
                'id': education.id,
                'education': education
            });
        }

        var init = function() {
            getEducation();
        };
        init();
    }])
