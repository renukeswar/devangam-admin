angular.module('clientApp')
    .controller('KalyanaMandapasCtrl', ['$scope', '$state', 'AdminService', '$mdDialog', 
    					function($scope, $state, AdminService, $mdDialog) {

        $scope.leaders = [];

        var getKalyanaMandapas = function() {
            AdminService.getKalyanaMandapas().then(function(response) {
                $scope.kalyanaMandapas = response;
                for (var i = 0, len = $scope.kalyanaMandapas.length; i < len; i++) {
                    $scope.kalyanaMandapas[i].imagePath = "app/images/kalyanaMandapas/" + $scope.kalyanaMandapas[i].imagePath;
                }
            })
        };

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                console.log(item)
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }
        
        getKalyanaMandapas();

        $scope.edit = function(leaders) {
            $state.go('editLeader', {
                'id': leaders.id,
                'leader': leaders
            });
        }

    }])
    .controller('AddKalyanaMandapasCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', 
    						function($scope, $state, AdminService, AlertService, $stateParams) {

        $scope.user = {

        };

        $scope.add = function() {
            $scope.isLoading = true;
            if ($scope.isNew) {
                AdminService.addKalyanaMandapas($scope.mandapam, $scope.logo.image).then(function(success) {
                    AlertService.alert("Addded Successfully")
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            } else {
                AdminService.updateKalyanaMandapas('updateCommunityLeaderDetails', $scope.user).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })

            }

        };

        $scope.back = function() {
            $state.go('leaders');
        }

        var init = function() {
            if ($stateParams.id) {
                $scope.isNew = false;
                $scope.user = $stateParams.leader;
                $scope.headerText = "Update Leader";
                    $scope.buttonText = "Update";
            } else {
                $scope.isNew = true;
                $scope.headerText = "Add Leader";
                    $scope.buttonText = "Add";
            }
        };
        init();
    }])
