angular.module('clientApp')
    .controller('DashboardCtrl', ['$scope', '$state', 'SessionService', 'AdminService', function($scope, $state, SessionService, AdminService) {

        $scope.users = [];
        $scope.userObj = {};

        $scope.gridOptions = {
            enableFiltering: true
        };

        $scope.gridOptions.columnDefs = [
            { name: 'name' },
            {name: 'mobileNumber'},
            { name: 'email', enableCellEdit: true },
            { name: 'district', enableCellEdit: true },
            { name: 'state', enableCellEdit: true }
        ];
        $scope.userLogout = function() {
            SessionService.deleteSession();
            $state.go('login')
        };
        var init = function() {
            AdminService.getUsers().then(function(response) {
                for (var i = 0, len = response.length; i < len; i++) {
                    response[i].name = response[i].firstname + " " + response[i].lastname;
                }
                $scope.gridOptions.data = response;
            }, function(error) {

            })
        };
        init();
    }])
