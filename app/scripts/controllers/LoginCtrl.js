angular.module('clientApp')
    .controller('LoginCtrl', ['$scope', '$state', 'LoginService', 'SessionService', 'AlertService', function($scope, $state, LoginService, SessionService, AlertService) {

        $scope.user = {};

        var getUser = function() {
            LoginService.getUser($scope.user.username).then(function(response) {
                if (response && response.status == 'success') {
                    var obj = {
                        "accessToken": $scope.user.token,
                        "username": $scope.user.username,
                        "user": response.userRequestDto
                    }
                    SessionService.setSession(obj);
                    $state.go('dashboard');
                }
            }, function(error) {
                console.log("Error Getting user details");
            })
        };

        $scope.validateuser = function() {
            LoginService.login($scope.user).then(function(response) {
                if (response.token && $scope.user.username == 'admin@admin.com') {
                    $scope.user.token = response.token;
                    getUser();
                } else {
                    AlertService.alert("Email and password did not match", 'md-warn');
                }
            }, function(response) {
                console.log(response);
            })

        };

        var init = function() {
            //console.log(SessionService.getSession())
        }
        init();
    }]);
