angular.module('clientApp')
    .controller('EventsCtrl', ['$scope', 'AdminService', '$state', '$mdDialog', function($scope, AdminService, $state, $mdDialog) {

        $scope.events = [];

        var getEvents = function() {
            AdminService.getEvents().then(function(response) {
                $scope.events = response;
            })
        };

        $scope.edit = function(event) {
            $state.go('editEvent', {
                'id': event.eventId,
                'event': event
            });
        }

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                AdminService.deleteEvents(item.eventId).then(function(response) {
                    if (response.status == 'success') {
                        AlertService.alert("Deleted Successfully", 'md-primary');
                        getEvents();
                    } else {
                        AlertService.alert("Error deleting", 'md-warn');
                    }
                })
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }

        getEvents();

    }])
    .controller('AddEventsCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', function($scope, $state, AdminService, AlertService, $stateParams) {

        $scope.user = {
            //"eventPostDate": new Date()
        };
        $scope.isLoading = false;
        $scope.isNew = true;

        $scope.add = function() {
            $scope.isLoading = true;
            var obj = {
                "eventPostDate": moment(new Date()).format('YYYY-MM-DD'),
                "eventName": $scope.user.eventName,
                "eventDescription": $scope.user.eventDescription,
                "hostedBy": $scope.user.hostedBy,
                "eventLaunchDate": moment($scope.user.eventLaunchDate).format('YYYY-MM-DD'),
                "eventPostEndDate": moment($scope.user.eventPostEndDate).format('YYYY-MM-DD')
            }
            if ($scope.isNew) {
                AdminService.addEvent(obj).then(function(success) {
                    $scope.back();
                    AlertService.alert("Addded Successfully")
                    $scope.isLoading = false;
                }, function(error) {
                    AlertService.alert("Something went Wrong");
                    $scope.isLoading = false;
                })
            } else {
                obj.id = $stateParams.id;
                obj.helpingHandType = 'EVENTS';
                AdminService.updateHelping('updateEvents', obj).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            }
        };

        $scope.back = function() {
            $state.go('events');
        }

        var init = function() {
            if ($stateParams.id) {
                if ($stateParams.event) {
                    $scope.isNew = false;
                    if ($stateParams.event.eventLaunchDate) {
                        $stateParams.event.eventLaunchDate = new Date($stateParams.event.eventLaunchDate);
                    }
                    if ($stateParams.event.eventPostEndDate) {
                        $stateParams.event.eventPostEndDate = new Date($stateParams.event.eventPostEndDate);
                    }

                    $scope.user = $stateParams.event;
                } else {
                    $scope.back();
                }
                $scope.headerText = "Update Event";
                $scope.buttonText = "Update";
            } else {
                $scope.headerText = "Add Event";
                $scope.buttonText = "Add";
                $scope.isNew = true;
            }
        };
        init();
    }])
