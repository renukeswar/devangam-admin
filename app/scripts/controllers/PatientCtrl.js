angular.module('clientApp')
    .controller('PatientCtrl', ['$scope', '$state', 'AdminService', '$state', '$mdDialog', 'AlertService', function($scope, $state, AdminService, $state, $mdDialog, AlertService) {

        var getPatients = function() {
            AdminService.getPatients().then(function(response) {
                $scope.patientsArr = response;
            }, function(error) {
                console.log("Error");
            })
        };

        $scope.edit = function(patient) {
            $state.go('editPatient', {
                'id': patient.id,
                'patient': patient
            });
        };

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                 AdminService.deletePatients(item.id).then(function(response) {
                    if (response.status == 'success'){
                        AlertService.alert("Deleted Successfully", 'md-primary');
                        getPatients();
                    }else{
                        AlertService.alert("Error deleting", 'md-warn');
                    }
                })
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }

        var init = function() {
            getPatients();
        };
        init();
    }])
    .controller('AddPatientCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', function($scope, $state, AdminService, AlertService, $stateParams) {

        $scope.user = {};
        $scope.isNew = true;
        $scope.isLoading = false;

        $scope.add = function() {
            $scope.isLoading = true;
            if ($scope.isNew) {
                AdminService.addPatients($scope.user).then(function(success) {
                    AlertService.alert("added Successfully", 'md-primary');
                    $scope.isLoading = false;
                    $scope.back();
                }, function(erorr) {

                })
            } else {
                $scope.user.helpingHandType = 'PATIENT';
                AdminService.updateHelping('editPatientHelpingHands', $scope.user).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            }
        };

        $scope.back = function() {
            $state.go('patient');
        }
        var init = function() {
            if ($stateParams.id) {
                if ($stateParams.patient) {
                    $scope.isNew = false;
                    $scope.user = $stateParams.patient;
                    $scope.user.phoneNumber = parseInt($scope.user.phoneNumber);
                    $scope.headerText = "Update Patient";
                    $scope.buttonText = "Update";
                } else {
                    $scope.back();
                }
            } else {
                $scope.isNew = true;
                $scope.headerText = "Add Patient";
                    $scope.buttonText = "Add";
            }
        };
        init();

    }])
