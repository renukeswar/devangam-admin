angular.module('clientApp')
    .controller('GalleryCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', 
    	function($scope, $state, AdminService, AlertService, $stateParams) {
    	
    	console.log("in add gallery controller");
    	  $scope.user = {};
          $scope.isLoading = false;
          $scope.isNew = true;
          $scope.logo = {};
          $scope.images = [];
      
          var getImages = function() {
              AdminService.getImages().then(function(response) {
                  $scope.images = response;
                  for (var i = 0, len = $scope.images.length; i < len; i++) {
                      //$scope.adds[i].imagePath ="http://www.devanga.org:9000/static/gallery/"+$scope.images[i].imagePath;
               	   $scope.images[i].imagePath ="/app/images/gallery/"+$scope.images[i].imagePath;
                      
                  }
              }, function(error) {

              })
          };
          $scope.delete = function(ev, item) {
              var confirm = $mdDialog.confirm()
                  .title('Would you like to delete?')
                  .textContent('')
                  .ariaLabel('Lucky day')
                  .targetEvent(ev)
                  .ok('Please do it!')
                  .cancel('Cancel');

              $mdDialog.show(confirm).then(function() {
                  AdminService.deleteImage(item.id).then(function(response) {
                      if (response.status == 'success') {
                          AlertService.alert("Deleted Successfully", 'md-primary');
                          getAdds();
                      } else {
                          AlertService.alert("Error deleting", 'md-warn');
                      }
                  })
              }, function() {
                  $scope.status = 'You decided to keep your debt.';
              });
          }
          $scope.back = function() {
              $state.go('gallery');
          }
          var init = function() {
       	   getImages();
          };
          init();
    	
    }])
    .controller('AddGalleryCtrl', ['$scope', 'AdminService', '$state', '$mdDialog', 'AlertService', '$stateParams', 
    	function($scope, AdminService, $state, $mdDialog, AlertService,$stateParams) {
    	console.log("in add gallery controller");
    	$scope.isNew = true;
           $scope.add = function() {
               if ($scope.isNew) {
               	AdminService.addImage($scope.gallery, $scope.logo.image).then(function(success) {
                       AlertService.alert("Addded Successfully");
                       $scope.isLoading = false;
                       $scope.back();
                   }, function(error) {
                       console.log("Error adding Adds");
                       $scope.isLoading = false;
                   })
               } else {
                   if($scope.logo.image){
                      // delete $scope.user.imagePath;
                	 
                   }
               	AdminService.updateImage($scope.gallery, $scope.logo.image).then(function(success) {
                       AlertService.alert("Updated Successfully", 'md-primary');
                       $scope.back();
                       $scope.isLoading = false;
                   }, function(erorr) {
                       $scope.isLoading = false;
                   })
               }
           };
           $scope.back = function() {
               $state.go('gallery');
           }
          
    
    }])

.directive( "fileModel", [ "$parse", function( $parse ) {
	return {
		restrict: "A",
		link: function( scope, element, attrs ) {

			var model = $parse( attrs.fileModel );

			element.bind( "change", function() {
				scope.$apply( function() {
					model.assign( scope, element[ 0 ].files[ 0 ] );
				});
			});

		}
	}
}]);