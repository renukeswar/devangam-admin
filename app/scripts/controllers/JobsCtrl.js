angular.module('clientApp')
    .controller('AddJobsCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', function($scope, $state, AdminService, AlertService, $stateParams) {

        $scope.user = {};

        $scope.add = function() {
            if ($scope.isNew) {
                AdminService.addJobs($scope.user).then(function(success) {
                    AlertService.alert("added Successfully", 'md-primary');
                    $scope.back();
                }, function(erorr) {

                })
            } else {
                AdminService.updateJobs($scope.user).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            }
        };

        $scope.back = function() {
            $state.go('jobs');
        }
        var init = function() {
            if ($stateParams.id) {
                if ($stateParams.job) {
                    $scope.isNew = false;
                    $scope.user = $stateParams.job;
                    $scope.headerText = "Update Job";
                    $scope.buttonText = "Update";
                } else {
                    $scope.back();
                }
            } else {
                $scope.isNew = true;
                $scope.headerText = "Add Job";
                $scope.buttonText = "Add";
            }
        };
        init();

    }])
    .controller('JobsCtrl', ['$scope', 'AdminService', '$state', '$mdDialog', function($scope, AdminService, $state, $mdDialog) {
        var getJobs = function() {
            AdminService.getJobs().then(function(response) {
                $scope.jobsArr = response;
            }, function(error) {

            })
        };

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                console.log(item)
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }

        $scope.edit = function(job) {
            $state.go('editJob', {
                'id': job.id,
                'job': job
            });
        }

        var init = function() {
            getJobs();
        };
        init();
    }])
