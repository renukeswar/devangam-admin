angular.module('clientApp')
    .controller('OldAgeHomeCtrl', ['$scope', '$state', 'AdminService', '$state', '$mdDialog', 'AlertService', function($scope, $state, AdminService, $state, $mdDialog, AlertService) {

        var getOldAgeHome = function() {
            AdminService.getOldAge().then(function(response) {
                $scope.oldAgeArr = response;
            }, function(error) {
                console.log("Error");
            })
        };

        $scope.edit = function(oldAge) {
            $state.go('editOldAge', {
                'id': oldAge.id,
                'oldAge': oldAge
            });
        };

        $scope.delete = function(ev, item) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                AdminService.deleteOldAge(item.id).then(function(response) {
                    if (response.status == 'success'){
                        AlertService.alert("Deleted Successfully", 'md-primary');
                        getOldAgeHome();
                    }else{
                        AlertService.alert("Error deleting", 'md-warn');
                    }
                })
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        }

        var init = function() {
            getOldAgeHome();
        };
        init();
    }])
    .controller('AddOldAgeHomeCtrl', ['$scope', '$state', 'AdminService', 'AlertService', '$stateParams', function($scope, $state, AdminService, AlertService, $stateParams) {

        $scope.user = {};
        $scope.isNew = true;
        $scope.isLoading = false;

        $scope.add = function() {
            $scope.isLoading = true;
            if ($scope.isNew) {
                AdminService.addOldAge($scope.user).then(function(success) {
                    AlertService.alert("added Successfully", 'md-primary');
                    $scope.back();
                }, function(erorr) {

                })
            } else {
                $scope.user.helpingHandType = 'OLDAGE';
                AdminService.updateHelping('editOldAgeHomeHelpingHands', $scope.user).then(function(success) {
                    AlertService.alert("Updated Successfully", 'md-primary');
                    $scope.back();
                    $scope.isLoading = false;
                }, function(erorr) {
                    $scope.isLoading = false;
                })
            }
        };

        $scope.back = function() {
            $state.go('oldAgeHome');
        }
        var init = function() {
            if ($stateParams.id) {
                if ($stateParams.oldAge) {
                    $scope.isNew = false;
                    $scope.user = $stateParams.oldAge;
                    $scope.user.phoneNumber = parseInt($scope.user.phoneNumber);
                    $scope.headerText = "Update Old Age Home";
                    $scope.buttonText = "Update";
                } else {
                    $scope.back();
                }
            } else {
                $scope.isNew = true;
                $scope.headerText = "Add Old Age Home";
                    $scope.buttonText = "Add";
            }
        };
        init();

    }])
