angular.module('clientApp')
    .factory('AdminService', ['$q', 'AdminFactory', 'SessionService', 'APP_CONFIG', '$http', function($q, AdminFactory, SessionService, APP_CONFIG, $http) {
        var adminServ = {};

        adminServ.getUsers = function() {
            var deferred = $q.defer();
            //console.log("her")
            AdminFactory.helpingHands().saveArr({ 'method': 'getRegisteredUsers' }, function(success) {
                console.log(success);
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.getEducation = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getEducationDetails' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.addEducation = function(params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': 'addEducationDetails' }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.deleteEducation = function(id) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().delete({ 'method': 'disableEducationDetailsById', 'helpingHandId': id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.addEvent = function(params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': 'saveEventDetails' }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.getEvents = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getListOfEvents' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.deleteEvents = function(id) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().delete({ 'method': 'disableEventById', 'id': id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.addOldAge = function(params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': 'addOldAgeHomeDetails' }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.deleteOldAge = function(id) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().delete({ 'method': 'disableOldAgeHomeDetailsById', 'helpingHandId': id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.getOldAge = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getOldAgeHomeDetails' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.addPatients = function(params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': 'addPatientDetails' }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.deletePatients = function(id) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().delete({ 'method': 'disablePatientDetailsById', 'helpingHandId': id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.getPatients = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getPatientDetails' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.addAdds = function(obj, file) {
            var deferred = $q.defer();
            var formData = new FormData();
            formData.append("advertisementRequestJson", JSON.stringify(obj));
            formData.append("files", file);
            $http.post(APP_CONFIG.API_URL + 'api/saveAdvertisementDetails', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.updateAdds = function(obj, file) {
            var deferred = $q.defer();
            var formData = new FormData();
            formData.append("advertisementRequestJson", JSON.stringify(obj));
            formData.append("files", file);
            $http.post(APP_CONFIG.API_URL + 'api/editAdevertisement', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.deleteAdd = function(id) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().delete({ 'method': 'disableAdvertisementDetails', 'id': id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        adminServ.deleteImage = function(id) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().delete({ 'method': 'deleteGalleryImages', 'id': id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.getAdds = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getAdvertisementDetails' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.addLeaders = function(obj, file) {
            var deferred = $q.defer();
            var formData = new FormData();
            formData.append("requestJson", JSON.stringify(obj));
            formData.append("file", file);
            $http.post(APP_CONFIG.API_URL + 'api/addCommunityLeaderDetails', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        /// Adding Kalyana Mandapas
        adminServ.addKalyanaMandapas = function(obj, file) {
            var deferred = $q.defer();
            var formData = new FormData();
            formData.append("kalyanaMandapaInfo", JSON.stringify(obj));
            formData.append("file", file);
            $http.post(APP_CONFIG.API_URL + 'api/saveKalynaMandapasInfo', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        
      
        adminServ.addImage = function(obj, file) {
        	 var deferred = $q.defer();
             var formData = new FormData();
             formData.append("requestJson", JSON.stringify(obj));
             formData.append("files", file);
             $http.post(APP_CONFIG.API_URL + 'api/addGalleryImages', formData, {
                 transformRequest: angular.identity,
                 headers: {
                     'Content-Type': undefined
                 }
             }).then(function(data) {
                 deferred.resolve(data);
             }, function(error) {
                 deferred.reject(error);
             });
             return deferred.promise;
    	
        };

        adminServ.getImages = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getGalleryImages' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        adminServ.getLeaders = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getCommunityLeaderDetails' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.updateLeader = function(url, params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': url }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };


        adminServ.addJobs = function(params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': 'saveJobDetails' }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.updateJobs = function(params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': 'updateJobDetails' }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.getJobs = function() {
            var deferred = $q.defer();
            AdminFactory.helpingHands().get({ 'method': 'getJobOpportunities' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        adminServ.updateHelping = function(url, params) {
            var deferred = $q.defer();
            AdminFactory.helpingHands().save({ 'method': url }, params, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        return adminServ;
    }])
    .factory('AdminFactory', ['$resource', 'APP_CONFIG', function($resource, APP_CONFIG) {

        var adminFact = {};
        adminFact.helpingHands = function() {
            return $resource(APP_CONFIG.API_URL + 'api/:method', {
                method: '@method'
            }, {
                'save': {
                    method: 'POST'
                },
                'saveArr': {
                    method: 'POST',
                    isArray: true
                },
                'get': {
                    method: 'GET',
                    isArray: true
                },
                'delete': {
                    method: 'DELETE'
                }

            })
        };


        return adminFact;
    }])
