angular.module('clientApp')
    .factory('SessionService', ['localStorageService', function(localStorageService) {

        this.setSession = function(session) {
            if (localStorageService.isSupported) {
                localStorageService.set("accessToken", session.accessToken);
                localStorageService.set("username", session.username);
                localStorageService.set("user", session.user);

            } else {
                localStorageService.cookie.set("accessToken", session.accessToken);
                localStorageService.cookie.set("username", session.username);
                localStorageService.cookie.set("user", session.user);
            }
        }

        this.getSession = function() {
            var session = {};
            if (localStorageService.isSupported) {
                session.accessToken = localStorageService.get("accessToken");
                session.username = localStorageService.get("username");
                session.user = localStorageService.get("user");
            } else {
                session.accessToken = localStorageService.cookie.get("accessToken");
                session.username = localStorageService.cookie.get("username");
                session.user = localStorageService.cookie.get("user");
            }
            return session;
        };

        this.updateSession = function(user) {
            if (localStorageService.isSupported) {
                localStorageService.set("user", user);
            } else {
                localStorageService.cookie.set("user", user);
            }
        }

        this.checkSession = function() {
            if (localStorageService.isSupported) {
                if (localStorageService.get("user")) {
                    return true;
                } else {
                    return false;
                };
            } else {
                if (localStorageService.cookie.get("user")) {
                    return true;
                } else {
                    return false;
                };
            }

        };

        this.getSessionObject = function() {
            return true;
        };

        this.getStoredUserToken = function() {
            if (localStorageService.isSupported) {
                return localStorageService.get("accessToken");
            } else {
                return localStorageService.cookie.get("accessToken");
            }
        };


        this.deleteSession = function() {
            if (localStorageService.isSupported) {
                localStorageService.clearAll();
            } else {
                localStorageService.cookie.clearAll();
            }
        };

        return this;

    }])
