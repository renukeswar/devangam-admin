'use strict';
/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */

angular.module('clientApp', [
        'ui.router',
        'ngResource',
        'ngCookies',
        'ngMaterial',
        'ngMessages',
        'angular-loading-bar',
        'ngAnimate',
        'LocalStorageModule',
        'ngFileUpload',
        'naif.base64',
        'http-auth-interceptor',
        'underscore',
        'md.data.table',
        'mdPickers',
        'ui.grid'
    ])
    .constant('APP_CONFIG', {
        //API_URL: 'http://122.175.36.113:8080/jyothigas/'
        API_URL: 'http://www.devanga.org:9000/'
            //API_URL: 'http://localhost:8080/'

    })
    .config(['$qProvider', function($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }])
    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        //cfpLoadingBarProvider.latencyThreshold = 100;
    }])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/login');
        $stateProvider
            .state('layout', {
                url: '',
                abstract: true,
                views: {
                    '': {
                        templateUrl: 'app/views/layout/layout.html',
                        controller: 'LayoutCtrl'
                    },
                    'header@layout': {
                        templateUrl: 'app/views/layout/header.html'
                    },
                    'nav@layout': {
                        templateUrl: 'app/views/layout/nav.html'
                    },
                    'footer@layout': {
                        templateUrl: 'app/views/layout/footer.html'
                    }
                }
            })
            .state('main', {
                url: '/',
                templateUrl: "app/views/landingPage.html",
                controller: 'LandingpageCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: "app/views/login.html",
                controller: "LoginCtrl",
                data: {
                    type: 'login'
                }
            })
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: "app/views/dashboard.html",
                controller: "DashboardCtrl",
                parent: 'layout',
                data: {

                    name: 'dashboard'
                }
            })
            .state('education', {
                url: '/education',
                templateUrl: "app/views/education.html",
                controller: "EducationCtrl",
                parent: 'layout',
                data: {

                    name: 'education'
                }
            })
            .state('addEducation', {
                url: '/addEducation',
                templateUrl: "app/views/addEducation.html",
                controller: "AddEducationCtrl",
                parent: 'layout',
                data: {

                    name: 'education'
                }
            })
            .state('editEducation', {
                url: '/editEducation/:id',
                templateUrl: "app/views/addEducation.html",
                controller: "AddEducationCtrl",
                parent: 'layout',
                data: {

                    name: 'education'
                },
                params: {
                    education: null
                }
            })
            .state('patient', {
                url: '/patient',
                templateUrl: "app/views/patient.html",
                controller: "PatientCtrl",
                parent: 'layout',
                data: {

                    name: 'patient'
                }
            })

        .state('addPatient', {
            url: '/addPatient',
            templateUrl: "app/views/addPatient.html",
            controller: "AddPatientCtrl",
            parent: 'layout',
            data: {
                name: 'patient'
            }
        })

        .state('editPatient', {
            url: '/editPatient/:id',
            templateUrl: "app/views/addPatient.html",
            controller: "AddPatientCtrl",
            parent: 'layout',
            data: {
                name: 'patient'
            },
            params: {
                patient: null
            }
        })

        .state('oldAgeHome', {
            url: '/oldAgeHome',
            templateUrl: "app/views/oldAgeHome.html",
            controller: "OldAgeHomeCtrl",
            parent: 'layout',
            data: {
                name: 'oldAgeHome'
            }
        })

        .state('editOldAge', {
            url: '/editOldAge/:id',
            templateUrl: "app/views/AddOldAgeHome.html",
            controller: "AddOldAgeHomeCtrl",
            parent: 'layout',
            data: {
                name: 'oldAgeHome'
            },
            params: {
                oldAge: null
            }
        })

        .state('AddOldAgeHome', {
            url: '/addOldAgeHome',
            templateUrl: "app/views/AddOldAgeHome.html",
            controller: "AddOldAgeHomeCtrl",
            parent: 'layout',
            data: {
                name: 'oldAgeHome'
            }
        })

        .state('events', {
            url: '/events',
            templateUrl: "app/views/events.html",
            controller: "EventsCtrl",
            parent: 'layout',
            data: {
                name: 'events'
            }
        })

        .state('addEvents', {
            url: '/addEvents',
            templateUrl: "app/views/addEvents.html",
            controller: "AddEventsCtrl",
            parent: 'layout',
            data: {
                name: 'events'
            }
        })

        .state('editEvent', {
            url: '/editEvent/:id',
            templateUrl: "app/views/addEvents.html",
            controller: "AddEventsCtrl",
            parent: 'layout',
            data: {
                name: 'events'
            },
            params: {
                event: null
            }
        })

        .state('advertisements', {
            url: '/advertisements',
            templateUrl: "app/views/advertisements.html",
            controller: "AdvertisementsCtrl",
            parent: 'layout',
            data: {
                name: 'advertisements'
            }
        })

        .state('addAdvertisements', {
                url: '/addAdvertisements',
                templateUrl: "app/views/addAdvertisements.html",
                controller: "AddAdvertisementsCtrl",
                parent: 'layout',
                data: {

                    name: 'advertisements'
                }
            })
            .state('editAdvertisement', {
                url: '/editAdvertisement/:id',
                templateUrl: "app/views/addAdvertisements.html",
                controller: "AddAdvertisementsCtrl",
                parent: 'layout',
                data: {

                    name: 'advertisements'
                },
                params: {
                    advertisement: null
                }
            }).state('leaders', {
                url: '/leaders',
                templateUrl: "app/views/leaders.html",
                controller: "LeadersCtrl",
                parent: 'layout',
                data: {

                    name: 'leaders'
                }
            })
            .state('addLeaders', {
                url: '/addLeaders',
                templateUrl: "app/views/addLeaders.html",
                controller: "AddLeadersCtrl",
                parent: 'layout',
                data: {

                    name: 'leaders'
                }
            })

        .state('editLeader', {
            url: '/editLeader/:id',
            templateUrl: "app/views/addLeaders.html",
            controller: "AddLeadersCtrl",
            parent: 'layout',
            data: {
                name: 'leaders'
            },
            params: {
                leader: null
            }
        })

        .state('jobs', {
            url: '/jobs',
            templateUrl: "app/views/jobs.html",
            controller: "JobsCtrl",
            parent: 'layout',
            data: {
                name: 'jobs'
            }
        })

        .state('addJobs', {
            url: '/addJobs',
            templateUrl: "app/views/addJobs.html",
            controller: "AddJobsCtrl",
            parent: 'layout',
            data: {
                name: 'jobs'
            }
        })

        .state('editJob', {
            url: '/editJob/:id',
            templateUrl: "app/views/addJobs.html",
            controller: "AddJobsCtrl",
            parent: 'layout',
            data: {
                name: 'jobs'
            },
            params: {
                job: null
            }
        })
        .state('gallery', {
            url: '/gallery',
            templateUrl: "app/views/gallery.html",
            controller: "GalleryCtrl",
            parent: 'layout',
            data: {
                name: 'gallery'
            }
        })

        .state('addGallery', {
            url: '/addGallery',
            templateUrl: "app/views/addGallery.html",
            controller: "AddGalleryCtrl",
            parent: 'layout',
            data: {
                name: 'gallery'
            }
        })
        // Kalyana mandapas added
        .state('kalyanamandapas', {
            url: '/kalyanamandapas',
            templateUrl: "app/views/kalyanamandapas.html",
            controller: "KalyanaMandapasCtrl",
            parent: 'layout',
            data: {
                name: 'kalyanamandapas'
            }
        })

        .state('addKalyanamandapas', {
            url: '/addKalyanamandapas',
            templateUrl: "app/views/addKalyanaMandapas.html",
            controller: "AddKalyanamandapasCtrl",
            parent: 'layout',
            data: {
                name: 'kalyanamandapas'
            }
        })

    }])
    .config(['$mdThemingProvider', function($mdThemingProvider) {

        //Angular Material Theme Configuration
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('red')

        $mdThemingProvider.theme('docs-dark', 'default')
            .primaryPalette('yellow')
            .accentPalette('green')
        $mdThemingProvider.theme('altTheme')
            .primaryPalette('blue')
    }])
    .run(function($state, $rootScope, $stateParams, LoginService, SessionService) {
        $rootScope.$on('$stateChangeStart', function(event, next, params) {
            if (LoginService.isAuthenticated()) {

                console.log("In .run(), app.js - User Authenticated");
                if (LoginService.hasSessionObject()) {
                    console.log("In .run(), app.js - User has session object");

                    if (angular.isDefined(next.data) && angular.isDefined(next.data.type) && next.data.type === 'login') {
                        event.preventDefault();
                        $state.go('dashboard');
                    } else {
                        //$state.go('dashboard');
                    }

                } else {
                    console.log("user does not have session object");
                    if (next.name !== 'main') {
                        event.preventDefault();
                        console.log("In .run(), app.js - User sent to loading page.");
                        $rootScope.nextState = {
                            next: next,
                            params: params
                        };

                        $state.go('main', {
                            redirect: true
                        });
                    }
                }

            } else {
                //console.log("In .run(), app.js - User NOT Authenticated");
                if (!angular.isDefined(next.data) || !angular.isDefined(next.data.type)) {
                    event.preventDefault();
                    $state.go('login');
                } else if (next.data && next.data.type == 'home') {
                    //Access the pages of type home
                } else if (angular.isDefined(next.data) && angular.isDefined(next.data.type) && next.data.type !== 'login') {
                    event.preventDefault();
                    $state.go('login');
                } else {
                    //Its already going to page of type login
                    //Do nothing
                }
            }
        });

    });
